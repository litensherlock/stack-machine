-------------------------------------------------------------------------------
-- combined RAM of two blocks.
-- 128 16 bit addresses divided into two 8 bit blocks.
-- author: Jakob Lövhall
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity return_stack is
  port (clk,we,we2 : in STD_LOGIC; 
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address, in_address2 : in std_logic_vector(7 downto 0);				 
				out_data, out_data2: out STD_LOGIC_VECTOR(15 downto 0));
end return_stack;

architecture return_stack_arc of return_stack is
	component return_stack_block is
	port (clk,we,we2 : in STD_LOGIC;
        out_data, out_data2: out STD_LOGIC_VECTOR(7 downto 0);
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address,in_address2 : in std_logic_vector(7 downto 0));
	end component;
  
	signal long_address, long_address2: std_logic_vector(7 downto 0);
		
	signal in_data_byte0,in_data_byte1 : std_logic_vector (7 downto 0);
	signal in_data2_byte0,in_data2_byte1 : std_logic_vector (7 downto 0);
	
	signal out_data_byte0,out_data_byte1 : std_logic_vector (7 downto 0);
	signal out_data2_byte0,out_data2_byte1 : std_logic_vector (7 downto 0);
	
	signal out_word : std_logic_vector (15 downto 0);
	signal out_word2 : std_logic_vector (15 downto 0);
	
  signal we_byte0,we_byte1 : std_logic;
	signal we2_byte0,we2_byte1 : std_logic;		
		
begin

	long_address <=in_address(7 downto 0);
	long_address2 <=in_address2(7 downto 0);
	
	--make write enable signals							
	we_byte1 <= we;
							
	we_byte0 <= we;
							
	--make write enable signals
	we2_byte1 <= we2;
							
	we2_byte0 <= we2;
							
	-- make in data bytes
	in_data_byte1 <= in_data (15 downto 8);
									 
	in_data_byte0 <= in_data (7 downto 0);
		
		-- make in data bytes
	in_data2_byte1 <= in_data2 (7 downto 0);
									 
	in_data2_byte0 <= in_data2 (7 downto 0);
		
	block0: ram_block port map (
		clk=>clk,
		we=>we_byte0,
		we2=>we2_byte0,
		in_data=>in_data_byte0,
		in_data2=>in_data2_byte0,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte0,
		out_data2=>out_data2_byte0		
		);
	
	block1: ram_block port map (
		clk=>clk,
		we=>we_byte1,
		we2=>we2_byte1,
		in_data=>in_data_byte1,
		in_data2=>in_data2_byte1,
		in_address=>long_address,
		in_address2=>long_address2,
		out_data=>out_data_byte1,
		out_data2=>out_data2_byte1		
		);		

	out_data <= out_data_byte1 & out_data_byte0;	
	
	out_data2 <= out_data2_byte1 & out_data2_byte0;
							
end return_stack_arc;
