-- author:Jakob Lövhall

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity stack_machine is
  port(clk : in std_logic;				 
			 iur : in std_logic;
			 reset : in std_logic;		
			 our : in std_logic;
			 data_from_PM : in std_logic_vector(31 downto 0);
			 return_stack_data_to_pc : in std_logic_vector(15 downto 0);
			 value_stack_data_to_bus : in std_logic_vector(31 downto 0);
			 address_to_data_memory : out std_logic_vector(15 downto 0);
			 data_to_PM : out std_logic_vector(31 downto 0);
			 write_enable_to_PM : out std_logic;
			 mode_out : out std_logic_vector (1 downto 0);
			 return_stack_pointer : out std_logic_vector(7 downto 0);
			 return_stack_data_from_pc : out std_logic_vector(15 downto 0);
			 return_stack_write_enable : out std_logic;
			 
			 value_stack_pointer : out std_logic_vector(11 downto 0);
			 value_stack_data_from_bus : out std_logic_vector(31 downto 0);
			 value_stack_write_enable : out std_logic;
			 
			 );
end stack_machine;

architecture stack_machine_arc of stack_machine is
		
	signal main_bus : std_logic_vector(31 downto 0);
	signal PC : std_logic_vector(15 downto 0);
	signal CSP : std_logic_vector(15 downto 0);
	signal ALU1 : std_logic_vector(31 downto 0);
	signal ALU2 : std_logic_vector(31 downto 0);
	signal Temp : std_logic_vector(31 downto 0);
	signal VSP : std_logic_vector(9 downto 0);
	signal RSP : std_logic_vector(7 downto 0);
	signal Mem_adr : std_logic_vector(31 downto 0);
	signal IR : std_logic_vector(7 downto 0);	
	signal mPC : Integer := 0;	
	signal m_mem_out : std_logic_vector(32 downto 0);	
	
	-- constants
	constant ADD_r :std_logic_vector (5 downto 0) := "000001";
	constant ADD_c :std_logic_vector (5 downto 0) := "000010";
	constant SUB_r :std_logic_vector (5 downto 0) := "000011";
	constant SUB_c :std_logic_vector (5 downto 0) := "000100";
	constant LOAD :std_logic_vector (5 downto 0) := "000101";
	constant STORE :std_logic_vector (5 downto 0) := "000110";
	constant CMP_r :std_logic_vector (5 downto 0) := "000111";
	constant JMP :std_logic_vector (5 downto 0) := "001000";
	constant JOour :std_logic_vector (5 downto 0) := "010001";
	constant JOZ :std_logic_vector (5 downto 0) := "001010";
	constant JNZ :std_logic_vector (5 downto 0) := "001011";
	constant JOC :std_logic_vector (5 downto 0) := "001100";
	constant JNC :std_logic_vector (5 downto 0) := "001101";
	
	constant VSP_inc : Integer := 21;
	constant VSP_dec : Integer := 22;
	
begin
	return_stack_pointer <= RSP;
--RSP
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(27) == '1' then
				CSP <= CSP - 1;
			else 
				if m_mem_out(26) == '1' then
					CSP <= CSP + 1;
				end if;
			end if;
    end if;
  end process;

--VSP
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(VSP_dec) == '1' then
				VSP <= VSP - 1;
			else 
				if m_mem_out(VSP_inc) == '1' then
					VSP <= VSP + 1;
				end if;
			end if;
    end if;
  end process;

--CSP
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(16) == '1' then
				CSP <= CSP - 1;
			else 
				if m_mem_out(30) == '1' then
					CSP <= CSP + 1;
				end if;
			end if;
    end if;
  end process;

--ALU1
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(15) == '1' then
				ALU1 <= main_bus;
			end if;
    end if;
  end process;
	
--ALU2
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(17) == '1' then
				ALU2 <= main_bus;
			end if;
    end if;
  end process
  
--Temp
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(19) == '1' then
				Temp <= main_bus;				
			else 
				if m_mem_out(18) == '1' then
					main_bus <= Temp;			
				end if;
			end if;			
    end if;
  end process
	
--Mem adr
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(7) == '1' then
				Mem_adr <= main_bus;
			end if;
    end if;
  end process
	
--IR
	process(clk)
  begin
    if rising_edge(clk) then      
			if m_mem_out(0) == '1' then
				IR <= main_bus(7 downto 0);
			end if;
    end if;
  end process
  
end stack_machine_arc;