-- author:Jakob L�vhall

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;



entity main is
  Port (clk, rx, rst : in STD_LOGIC;
	tx : out std_logic;
	vga, seg: out STD_LOGIC_VECTOR(7 downto 0);
	an: out STD_LOGIC_VECTOR(3 downto 0);
	Led : out std_logic_vector (7 downto 0));
end main; 

architecture main_arc of main is

	component IO is
			Port ( clk,rst, rx : in  STD_LOGIC;
					new_out_data : in boolean;
					tx : out std_logic;
					our : out std_logic;
          seg: out  STD_LOGIC_VECTOR(7 downto 0);
          an : out  STD_LOGIC_VECTOR (3 downto 0);
					to_leds : in std_logic_vector( 15 downto 0);
					out_uart : in std_logic_vector (7 downto 0);
					address_out : out std_logic_vector (13 downto 0);
					instruction : out STD_LOGIC_VECTOR(31 downto 0);-- data out
					we_memory : out std_logic;
					CPU_reset : out std_logic;
					in_reg : out std_logic_vector (7 downto 0);
					load_puls_out : out std_logic);
	end component;

  component stack_machine is
   port(clk : in std_logic;				 
			 iur : in std_logic;
			 reset : in std_logic;		
			 our : in std_logic;
			 data_from_PM : in std_logic_vector(31 downto 0);
			 return_stack_data_to_pc : in std_logic_vector(15 downto 0);
			 value_stack_data_to_bus : in std_logic_vector(31 downto 0);
			 address_to_data_memory : out std_logic_vector(15 downto 0);
			 data_to_PM : out std_logic_vector(31 downto 0);
			 write_enable_to_PM : out std_logic;
			 mode_out : out std_logic_vector (1 downto 0);
			 return_stack_pointer : out std_logic_vector(7 downto 0);
			 return_stack_data_from_pc : out std_logic_vector(15 downto 0);
			 return_stack_write_enable : out std_logic;
			 
			 value_stack_pointer : out std_logic_vector(11 downto 0);
			 value_stack_data_from_bus : out std_logic_vector(31 downto 0);
			 value_stack_write_enable : out std_logic;
			 
			 );
  end component;

	component data_ram is
		port (clk,we,we2 : in STD_LOGIC;
					new_out_data : out boolean;
					in_uart_ready : in std_logic;					
					mode2 : in std_logic_vector (1 downto 0);
					out_data, out_data2: out STD_LOGIC_VECTOR(31 downto 0);
					in_data, in_data2: in STD_LOGIC_VECTOR(31 downto 0);					
					in_uart : in std_logic_vector (7 downto 0);
					in_address : in std_logic_vector ( 13 downto 0);
					in_address2 : in std_logic_vector(15 downto 0);
					to_uart : out std_logic_vector (7 downto 0);
					iur : out std_logic);
	end component;
	
	component micro_code_memory is
		port (clk,we,we2 : in STD_LOGIC;
        out_data, out_data2: out STD_LOGIC_VECTOR(7 downto 0);
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address,in_address2 : in std_logic_vector(7 downto 0));
	end component;
	
	component return_stack is
		port (clk,we,we2 : in STD_LOGIC; 
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address, in_address2 : in std_logic_vector(7 downto 0);				 
				out_data, out_data2: out STD_LOGIC_VECTOR(15 downto 0));
	end component;
	
	component value_stack is
		port (clk,we,we2 : in STD_LOGIC;
				mode, mode2 : in std_logic_vector (1 downto 0);
				in_data, in_data2: in STD_LOGIC_VECTOR(31 downto 0);
				in_address, in_address2 : in std_logic_vector(11 downto 0);				
				out_data, out_data2: out STD_LOGIC_VECTOR(31 downto 0));
	end component;

  component REGS is
    port(clk, we, we_jstk, stall_from_cpu : in std_logic;
         rst: in STD_LOGIC;
         data3_in, data_jstk_in: in STD_LOGIC_VECTOR(31 downto 0);
         addr1, addr2, addr3: in STD_LOGIC_VECTOR(4 downto 0);
         
         data1_out, data2_out: out STD_LOGIC_VECTOR(31 downto 0); 
				 reg0_out : out std_logic_vector(13 downto 0);				 

         pal0, pal1, pal2, pal3: out std_logic_vector(7 downto 0));
  end component;

  signal IO_address, write_address_from_cpu,
					read_address_from_cpu: std_logic_vector (13 downto 0);
	
	signal data_address_from_cpu, rsdfp, rsdtp : std_logic_vector (15 downto 0);
	signal rsp : std_logic_vector (7 downto 0);
					
  signal IO_data, GU_data, regs_data_out_from_cpu,other_in_data: STD_LOGIC_VECTOR(31 downto 0);
	
  signal cpu_instruction, cpu_data_in, data_to_cpu, data_from_cpu, data_from_reg_1,
					data_from_reg_2: std_logic_vector (31 downto 0);
					
  signal we_memory, cpu_reset, GU_using_memory,
				we_from_cpu,write_enable_regs_from_cpu,joystick_read,jstk_write_enable_regs,
				stall_register: std_logic;

  signal pal0, pal1, pal2, pal3, pal4, pal5, pal6, pal7,
				pal8, pal9, pal10, pal11, pal12, pal13, pal14, pal15,out_uart: STD_LOGIC_VECTOR(7 downto 0);

  signal reg_addr_1_from_cpu, reg_addr_2_from_cpu, reg_addr_3_from_cpu: STD_LOGIC_VECTOR(4 downto 0);
	signal data_mode : std_logic_vector (1 downto 0);
  -- interna signaler
	signal to_leds: std_logic_vector (15 downto 0);
	signal reg0 : std_logic_vector (13 downto 0);

  signal cpu_reset_signal, new_data_in_uart, iur,our, rswe : std_logic;
	signal new_out_data : boolean;
	signal in_uart : std_logic_vector (7 downto 0);
begin
	
  cpu_reset_signal <= rst or cpu_reset;   
  
	to_leds<=pal2 & pal3;
	
  Led <= pal1;

  IO_comp: IO port map (
    --in
    clk=>clk,
    rst=>rst,
    rx=>rx,
    to_leds => to_leds,
		new_out_data=>new_out_data,
		out_uart=>out_uart,
		
    --out
		tx=>tx,
		our=>our,
    seg=>seg,
    an=>an,
		address_out=>IO_address,
		instruction=>IO_data,
		cpu_reset=>cpu_reset,
		we_memory=>we_memory,
		in_reg=>in_uart,
		load_puls_out=>new_data_in_uart
    );

 	
  stack_machine_comp: stack_machine port map(
		--in
		iur=>iur,
    clk => clk,
		our=>our,
    reset => cpu_reset_signal,
    data_from_PM => data_to_cpu,
		return_stack_data_to_pc => rsdtp,
		
		--out
		data_to_PM => data_from_cpu,		
		write_enable_to_PM => we_from_cpu,
		address_to_PM => data_address_from_cpu,
		mode_out => data_mode,
		return_stack_pointer => rsp,
		return_stack_data_from_pc => rsdfp,
		return_stack_write_enable => rswe
		
		value_stack_pointer => vsp,
		value_stack_data_from_bus => vsdfb, 
		value_stack_write_enable => vswe 
    );

	data_ram_comp: data_ram port map (
		--in
		clk=> clk,
		we=>we_memory,
		we2 =>we_from_cpu,
		mode2=> data_mode,
		in_data=> IO_data,
		in_data2=>data_from_cpu,
		in_address=>IO_address,
		in_address2=>data_address_from_cpu,
		in_uart_ready=>new_data_in_uart,
		in_uart=>in_uart,
		
		--out
		out_data=>cpu_instruction,
		out_data2=>data_to_cpu,
		to_uart=>out_uart,
		new_out_data=>new_out_data,
		iur=>iur		
		);
		
	return_stack_comp: return_stack port map (
		--in
		clk => clk,
		we => rswe,
		we2 => '0', --dummy
		in_data => rsdfp,
		in_data2 => "0000000000000000", -- dummy
		in_address => rsp,
		in_address2 => "00000000", --dummy
		
		--out
		out_data => rsdtp,
		out_data2
		);
		
	value_stack_comp : value_stack port map (
		--in
		clk => clk,
		we => vswe,
		we2 => '0', --dummy
		in_data => vsdfb,
		in_data2 => "00000000000000000000000000000000", --dummy
		in_address => vsp,
		in_address2 => "0000000000", -- dummy
		
		--out
		out_data => vsdtb,
		out_data2
		);
		
	micro_code_memory_comp : micro_code_memory port map (
		clk => clk,
		we,
		we2,
		out_data,
		out_data2,
		in_data, 
		in_data2,
		in_address,
		in_address2
		);
		  
end caesium_arc;
