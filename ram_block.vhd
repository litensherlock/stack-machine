-------------------------------------------------------------------------------
-- author: Jakob Lövhall
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ram_block is
  port (clk,we,we2 : in STD_LOGIC;
        out_data, out_data2: out STD_LOGIC_VECTOR(7 downto 0);
				in_data, in_data2: in STD_LOGIC_VECTOR(7 downto 0);
				in_address,in_address2 : in std_logic_vector(13 downto 0));
end ram_block;

architecture ram_block_arc of ram_block is

  type byte_ram is array(0 to 15295) of STD_LOGIC_VECTOR(7 downto 0);
	
	shared variable memory: byte_ram := (others => (others => '0'));

  signal out_data_int,out_data2_int : std_logic_vector (7 downto 0);

begin

--port1
	process(clk)
  begin
    if rising_edge(clk) then			
			out_data <= memory(to_integer(unsigned(in_address)));
			if we = '1' then
				memory(to_integer(unsigned(in_address))) := in_data;
			end if;
    end if;
  end process;
	
	--port 2
	process(clk)
  begin
    if rising_edge(clk) then			
			out_data2 <= memory(to_integer(unsigned(in_address2)));
			if we2 = '1' then
				memory(to_integer(unsigned(in_address2))) := in_data2;
			end if;
    end if;
  end process;
					
end ram_block_arc;
